package ec.telconet.mscomppruebaguillermoacuna.usuario.entity.model;

import java.util.Date;

import ec.telconet.mscomppruebaguillermoacuna.usuario.entity.request.UsuarioRequest;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "usuario")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UsuarioEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false, unique = true)
	private Integer id;
	
	@Column(name = "name", nullable = false, length = 50)
	private String nombre;
	
	@Column(name = "lastname", nullable = false, length = 50)
	private String apellido;
	
	@Column(name = "usarname", nullable = false, length = 50)
	private String usuario;
	
	@Column(name = "password", nullable = false, length = 500)
	private String clave;
	
	@Column(name = "mail", nullable = false, length = 50)
	private String email;
	
	@Column(name = "status", nullable = false, length = 1)
	private Character estado;
	
	@Column(name = "administrator", nullable = false)
	private Boolean admin;
	
	/*private Date fachaCreacion;
	
	private Date fechaActualizaion;
	
	private Integer idUsuarioCreacion;
	
	private Integer idUsuarioActualizacion;*/
	
	public UsuarioEntity(UsuarioRequest data) {
		this.nombre = data.getNombre();
		this.apellido = data.getApellido();
		this.clave = data.getContrasena();
		this.email = data.getCorreo();
		this.usuario = data.getUsuario();
		this.estado = 'A';
		this.admin = false;
	}
}
