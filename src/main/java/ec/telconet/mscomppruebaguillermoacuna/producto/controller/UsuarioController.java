package ec.telconet.mscomppruebaguillermoacuna.producto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.telconet.mscomppruebaguillermoacuna.producto.service.UsuarioService;
import ec.telconet.mscomppruebaguillermoacuna.usuario.entity.request.UsuarioRequest;
import ec.telconet.mscomppruebaguillermoacuna.usuario.entity.response.UsuarioResponse;
import ec.telconet.mscomppruebaguillermoacuna.util.entity.OutputEntity;

@RestController
@RequestMapping("/user")
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;
	
	@GetMapping
	public ResponseEntity<OutputEntity<List<UsuarioResponse>>> getAll(){
		OutputEntity<List<UsuarioResponse>> out = null;
		try {
			out = this.usuarioService.getAll();
			return new ResponseEntity<>(out, out.getCode());			
		} catch (Exception e) {
			out = new OutputEntity<List<UsuarioResponse>>().error();
			return new ResponseEntity<>(out, out.getCode());	
		}
	}
	
	
	@PostMapping
	public ResponseEntity<OutputEntity<String>> create(@RequestBody UsuarioRequest data){
		OutputEntity<String> out = null;
		try {
			out = this.usuarioService.create(data);
			return new ResponseEntity<>(out, out.getCode());
		} catch (Exception e) {
			out = new OutputEntity<String>().error();
			return new ResponseEntity<>(out, out.getCode());
		}
	}
}


