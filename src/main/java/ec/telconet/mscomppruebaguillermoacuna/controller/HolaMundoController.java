package ec.telconet.mscomppruebaguillermoacuna.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hola-mundo")
public class HolaMundoController {

	@GetMapping
	public String holaMundo() {
		return "Hola mundo";
	}
	
}
