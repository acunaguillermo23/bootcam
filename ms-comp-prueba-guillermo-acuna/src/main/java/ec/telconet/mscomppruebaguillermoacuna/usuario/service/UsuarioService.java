package ec.telconet.mscomppruebaguillermoacuna.usuario.service;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import ec.telconet.mscomppruebaguillermoacuna.producto.repository.UsuarioRepository;
import ec.telconet.mscomppruebaguillermoacuna.usuario.entity.model.UsuarioEntity;
import ec.telconet.mscomppruebaguillermoacuna.usuario.entity.request.UsuarioRequest;
import ec.telconet.mscomppruebaguillermoacuna.usuario.entity.response.UsuarioResponse;
import ec.telconet.mscomppruebaguillermoacuna.util.entity.OutputEntity;
import ec.telconet.mscomppruebaguillermoacuna.util.enums.MessageEnum;
import ec.telconet.mscomppruebaguillermoacuna.util.exception.MyException;
import ec.telconet.mscomppruebaguillermoacuna.util.helper.MetodoHelper;

@Service
public class UsuarioService {

	@Value("${ec.message}")
	private String perfil;

	@Value("${ec.secret-key-password}")
	private String secretKeyPassword;

	@Autowired
	private UsuarioRepository usuarioRepository;

	public OutputEntity<List<UsuarioResponse>> getAll() {
		OutputEntity<List<UsuarioResponse>> outPut = new OutputEntity<>();
		try {
			List<UsuarioEntity> usuarioModelo = this.usuarioRepository.findAll();

			if (usuarioModelo.isEmpty())
				throw new MyException(MessageEnum.NOT_FOUND.getCode(), MessageEnum.NOT_FOUND.getMensaje());

			List<UsuarioResponse> usuarioResponses = usuarioModelo.stream().map(u -> new UsuarioResponse(u))
					.collect(Collectors.toList());

			return outPut.ok(MessageEnum.OK.getCode(), MessageEnum.OK.getMensaje(), usuarioResponses);
		} catch (MyException e) {
			return outPut.error(e.getCode(), e.getMensaje(), null);
		} catch (Exception e) {
			return outPut.error();
		}
	}

	public OutputEntity<String> create(UsuarioRequest data) {
		OutputEntity<String> outPut = new OutputEntity<>();
		try {
			// TODO validar correo válido

			if (!MetodoHelper.isValidEmail(data.getCorreo()))
				throw new MyException(MessageEnum.CORREO_NO_VALIDO.getCode(),
						MessageEnum.CORREO_NO_VALIDO.getMensaje());

			// TODO NOMBRE SEA EN MAYÚSCULA
			MetodoHelper.isUpperCase(data.getNombre());

			// TODO LA CLAVE SEA SEGURA
			MetodoHelper.isStronge(data.getContrasena());

			// TODO ENCRYTAR LA CLAVE
			data.setContrasena(MetodoHelper.encryptPassword(data.getContrasena(), this.secretKeyPassword));

			UsuarioEntity usuarioEntity = new UsuarioEntity(data);
			// TODO GUARDAMOS
			this.usuarioRepository.save(usuarioEntity);
			return outPut.ok(MessageEnum.CREATE.getCode(), MessageEnum.CREATE.getMensaje(), null);
		} catch (MyException e) {
			return outPut.error(e.getCode(), e.getMensaje(), null);
		} catch (Exception e) {
			return outPut.error();
		}

	}

	public OutputEntity<List<UsuarioResponse>> findByName(String name) {
		OutputEntity<List<UsuarioResponse>> outPut = new OutputEntity<>();
		try {

			List<UsuarioEntity> usuarioModelo = this.usuarioRepository.findByName(name);

			if (usuarioModelo.isEmpty())
				throw new MyException(MessageEnum.NOT_FOUND.getCode(), MessageEnum.NOT_FOUND.getMensaje());

			List<UsuarioResponse> usuarioResponses = usuarioModelo.stream().map(u -> new UsuarioResponse(u))
					.collect(Collectors.toList());

			return outPut.ok(MessageEnum.OK.getCode(), MessageEnum.OK.getMensaje(), usuarioResponses);

		} catch (MyException e) {
			return outPut.error(e.getCode(), e.getMensaje(), null);
		} catch (Exception e) {
			return outPut.error();
		}
	}
	
	public OutputEntity<List<UsuarioResponse>> findByUser(String user) {
		OutputEntity<List<UsuarioResponse>> outPut = new OutputEntity<>();
		try {

			List<UsuarioEntity> usuarioModelo = this.usuarioRepository.findByUser(user);

			if (usuarioModelo.isEmpty())
				throw new MyException(MessageEnum.NOT_FOUND.getCode(), MessageEnum.NOT_FOUND.getMensaje());

			List<UsuarioResponse> usuarioResponses = usuarioModelo.stream().map(u -> new UsuarioResponse(u))
					.collect(Collectors.toList());

			return outPut.ok(MessageEnum.OK.getCode(), MessageEnum.OK.getMensaje(), usuarioResponses);

		} catch (MyException e) {
			return outPut.error(e.getCode(), e.getMensaje(), null);
		} catch (Exception e) {
			return outPut.error();
		}
	}
	
	public OutputEntity<List<UsuarioResponse>> findByMail(String mail){
		OutputEntity<List<UsuarioResponse>> outPut = new OutputEntity<>();
		try {
			List<UsuarioEntity> usuarioModelo = this.usuarioRepository.findByMail(mail);
			
			if(usuarioModelo.isEmpty())
				throw new MyException(MessageEnum.NOT_FOUND.getCode(), MessageEnum.NOT_FOUND.getMensaje());

			List<UsuarioResponse> usuarioResponses = usuarioModelo.stream().map(u -> new UsuarioResponse(u))
					.collect(Collectors.toList());

			return outPut.ok(MessageEnum.OK.getCode(), MessageEnum.OK.getMensaje(), usuarioResponses);

		} catch (MyException e) {
			return outPut.error(e.getCode(), e.getMensaje(), null);
		} catch (Exception e) {
			return outPut.error();
		}
	}
	
	
	public OutputEntity<List<UsuarioResponse>> getLastUser(Integer paginaInicial, Integer paginaFinal){
		OutputEntity<List<UsuarioResponse>> outPut = new OutputEntity<>();
		try {
			
			Pageable pageable = PageRequest.of(paginaInicial, paginaFinal, Sort.by("id").descending());
			List<UsuarioEntity> usuarioModelo = this.usuarioRepository.findAll(pageable).getContent();
			
			if(usuarioModelo.isEmpty())
				throw new MyException(MessageEnum.NOT_FOUND.getCode(), MessageEnum.NOT_FOUND.getMensaje());

			List<UsuarioResponse> usuarioResponses = usuarioModelo.stream().map(u -> new UsuarioResponse(u))
					.collect(Collectors.toList());

			return outPut.ok(MessageEnum.OK.getCode(), MessageEnum.OK.getMensaje(), usuarioResponses);
				
		} catch (MyException e) {
			return outPut.error(e.getCode(), e.getMensaje(), null);
		} catch (Exception e) {
			return outPut.error();
		}
	}
}
