package ec.telconet.mscomppruebaguillermoacuna;

import java.util.Arrays;
import java.util.Collections;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;


@SpringBootApplication
public class MsCompPruebaGuillermoAcunaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsCompPruebaGuillermoAcunaApplication.class, args);
	}

	@Bean
	public CorsFilter corsFiler() {
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		CorsConfiguration config = new CorsConfiguration();
		config.setAllowCredentials(true);
		config.setAllowedOriginPatterns(Collections.singletonList("*"));
		config.setAllowedHeaders(Arrays.asList("*"));
		config.addAllowedMethod("POST");
		source.registerCorsConfiguration("/**", config);		
		return new CorsFilter(source);
	}
}
