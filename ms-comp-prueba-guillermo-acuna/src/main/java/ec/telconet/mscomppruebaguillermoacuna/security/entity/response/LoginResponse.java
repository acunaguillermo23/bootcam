package ec.telconet.mscomppruebaguillermoacuna.security.entity.response;

import ec.telconet.mscomppruebaguillermoacuna.usuario.entity.response.UsuarioResponse;
import lombok.Data;

@Data
public class LoginResponse {

	public UsuarioResponse usuario;
	// TODO ENTIDAD DE LAS OPCIONES DEL SISTEMA
	// private List<OpcionesResponse> opciones = new ArrayList<>();
	
	public String token;
	
	public void mapData(UsuarioResponse usuario, String token) {
		this.usuario = usuario;
		this.token = token;
	}
}
