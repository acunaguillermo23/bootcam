package ec.telconet.mscomppruebaguillermoacuna.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import ec.telconet.mscomppruebaguillermoacuna.producto.repository.UsuarioRepository;
import ec.telconet.mscomppruebaguillermoacuna.security.entity.request.LoginRequest;
import ec.telconet.mscomppruebaguillermoacuna.security.entity.response.LoginResponse;
import ec.telconet.mscomppruebaguillermoacuna.usuario.entity.model.UsuarioEntity;
import ec.telconet.mscomppruebaguillermoacuna.usuario.entity.response.UsuarioResponse;
import ec.telconet.mscomppruebaguillermoacuna.util.entity.OutputEntity;
import ec.telconet.mscomppruebaguillermoacuna.util.enums.MessageEnum;
import ec.telconet.mscomppruebaguillermoacuna.util.exception.MyException;
import ec.telconet.mscomppruebaguillermoacuna.util.helper.MetodoHelper;
import ec.telconet.mscomppruebaguillermoacuna.util.helper.TokenHelper;

@Service
public class LoginService {
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private TokenHelper tokenHelper;
	
	@Value("${ec.secret-key-password}")
	private String secretKeyPassword;
	
	public OutputEntity<LoginResponse> login(LoginRequest data){
		OutputEntity<LoginResponse> outPut = new OutputEntity<>();
		try {
			
			LoginResponse login = new LoginResponse();
			
			UsuarioEntity usuarioModelo = this.usuarioRepository.findByUserLogin(data.getUsuario());
			
			if(usuarioModelo == null)
				throw new MyException(MessageEnum.NOT_FOUND.getCode(), MessageEnum.NOT_FOUND.getMensaje());
			
			// TODO COMPRAR LA CLAVE
			String password = MetodoHelper.desencryptPass(usuarioModelo.getClave(), this.secretKeyPassword);
			
			UsuarioResponse usuario = new UsuarioResponse(usuarioModelo);
			
			if(!StringUtils.pathEquals(password, data.getClave()))
				throw new MyException(MessageEnum.LOGIN_ERROR.getCode(), MessageEnum.LOGIN_ERROR.getMensaje());
			
			login.setUsuario(usuario);
			login.setToken(this.tokenHelper.generateToken(data.getUsuario(), usuario, "clave123"));
			
			return outPut.ok(MessageEnum.OK.getCode(), MessageEnum.OK.getMensaje(), login);
		} catch (MyException e) {
			return outPut.error(e.getCode(), e.getMensaje(), null);
		} catch (Exception e) {
			return outPut.error();
		}
	}

}
